package org.bitbucket.guwenk.tasktracker.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.bitbucket.guwenk.tasktracker.Constants;
import org.bitbucket.guwenk.tasktracker.DataManager;
import org.bitbucket.guwenk.tasktracker.R;
import org.bitbucket.guwenk.tasktracker.Tasks.TaskItem;
import org.bitbucket.guwenk.tasktracker.ui.task_details.TaskDetailsActivity;
import org.bitbucket.guwenk.tasktracker.ui.task_edit.TaskEditActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private FloatingActionButton buttonAdd;
    private RecyclerView recyclerView;
    private RVAdapter recyclerAdapter;
    private String currentFilter = "";
    private ArrayList<TaskItem> taskItemArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttonAdd = findViewById(R.id.fab);
        recyclerView = findViewById(R.id.actMain_RecyclerView);

        buildRecyclerView();

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TaskEditActivity.class);
                intent.setAction(Constants.ACTIONS.ADD);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateTaskArrayList(new DataManager(this).getTaskList());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        switch (currentFilter) {
            case Constants.STRING_TASK_STATUS.NEW:
                menu.findItem(R.id.sort_new).setChecked(true);
                break;
            case Constants.STRING_TASK_STATUS.IN_PROGRESS:
                menu.findItem(R.id.sort_in_progress).setChecked(true);
                break;
            case Constants.STRING_TASK_STATUS.COMPLETE:
                menu.findItem(R.id.sort_complete).setChecked(true);
                break;
            default:
                menu.findItem(R.id.sort_all).setChecked(true);
                break;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.sort_all:
                currentFilter = "";
                break;
            case R.id.sort_new:
                currentFilter = Constants.STRING_TASK_STATUS.NEW;
                break;
            case R.id.sort_in_progress:
                currentFilter = Constants.STRING_TASK_STATUS.IN_PROGRESS;
                break;
            case R.id.sort_complete:
                currentFilter = Constants.STRING_TASK_STATUS.COMPLETE;
                break;
        }
        recyclerAdapter.getFilter().filter(currentFilter);

        invalidateOptionsMenu();
        return super.onOptionsItemSelected(item);
    }

    public void updateTaskArrayList(ArrayList<TaskItem> arrayList) {
        taskItemArrayList.clear();
        taskItemArrayList.addAll(arrayList);
        recyclerAdapter.notifyDataSetChanged();
        recyclerAdapter.getFilter().filter(currentFilter);
    }

    void buildRecyclerView() {
        recyclerAdapter = new RVAdapter(this, taskItemArrayList);
        recyclerView.setAdapter(recyclerAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerAdapter.setOnItemClickListener(new RVAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int rawPosition) {
                int position = new DataManager(MainActivity.this).find_position_by_timestamp(recyclerAdapter.getFilteredTasks().get(rawPosition).getTimeStamp());
                Log.d("POSITION", "raw = " + rawPosition + "  ||  real = " + position);
                Intent intent = new Intent(MainActivity.this, TaskDetailsActivity.class);
                intent.putExtra(Constants.TASK_POS, position);
                startActivity(intent);
            }
        });
    }
}
