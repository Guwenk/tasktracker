package org.bitbucket.guwenk.tasktracker.ui.task_edit;

import android.content.Intent;
import android.view.View;

import org.bitbucket.guwenk.tasktracker.Constants;
import org.bitbucket.guwenk.tasktracker.DataManager;
import org.bitbucket.guwenk.tasktracker.R;
import org.bitbucket.guwenk.tasktracker.Tasks.TaskItem;

import java.util.ArrayList;
import java.util.GregorianCalendar;

class TaskEditPresenter {
    private TaskEditView mView;
    private int position;
    private Intent intent;

    private TaskItem taskItem;

    TaskEditPresenter(TaskEditView mView, Intent intent) {
        this.mView = mView;
        taskItem = new TaskItem();
        this.intent = intent;

        switch (intent.getAction()) {
            case Constants.ACTIONS.EDIT:
                position = intent.getExtras().getInt(Constants.TASK_POS);
                mView.buttonDeleteVisibility(View.VISIBLE);
                break;
            case Constants.ACTIONS.SHARE:
                mView.updateText(intent.getStringExtra(Intent.EXTRA_TEXT));
                break;
        }

    }

    void onApplyClick(String title, int checkedRadioButtonID, String text) {
        if (title.isEmpty()) {
            mView.wrongTitle();
            return;
        }
        if (taskItem.getDate() == null) {
            mView.wrongDate();
            return;
        }

        short status = Constants.TASK_STATUS.NEW;
        switch (checkedRadioButtonID) {
            case R.id.actTaskEdit_radioBtn_new:
                status = Constants.TASK_STATUS.NEW;
                break;
            case R.id.actTaskEdit_radioBtn_inProgress:
                status = Constants.TASK_STATUS.IN_PROGRESS;
                break;
            case R.id.actTaskEdit_radioBtn_complete:
                status = Constants.TASK_STATUS.COMPLETE;
                break;
        }
        taskItem.setTitle(title);
        taskItem.setStatus(status);
        taskItem.setText(text);

        ArrayList<TaskItem> arrayList = new DataManager(mView.getAppContext()).getTaskList();

        if (intent.getAction().equals(Constants.ACTIONS.EDIT))
            arrayList.set(position, taskItem);
        else
            arrayList.add(taskItem);

        new DataManager(mView.getAppContext()).saveTaskList(arrayList);

        mView.showShortToast(mView.getAppContext().getString(R.string.applied));
        mView.finishActivity();
    }

    void onDeleteClick() {
        ArrayList<TaskItem> arrayList = new DataManager(mView.getAppContext()).getTaskList();
        arrayList.remove(position);
        new DataManager(mView.getAppContext()).saveTaskList(arrayList);
        mView.finishActivity();
    }


    void onDateEditClick(GregorianCalendar gregorianCalendar) {
        taskItem.setDate(gregorianCalendar);
    }

    void loadDataByPosition() {
        TaskItem task = new DataManager(mView.getAppContext()).getTaskList().get(position);
        mView.updateTitle(task.getTitle());
        mView.updateDate(task.getDate());
        mView.updateStatus(task.getStatus());
        mView.updateText(task.getText());
        taskItem = task;
    }

}
