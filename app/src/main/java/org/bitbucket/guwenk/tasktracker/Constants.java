package org.bitbucket.guwenk.tasktracker;

import android.content.Intent;

public interface Constants {
    interface TASK_STATUS {
        short NEW = 0;
        short IN_PROGRESS = 1;
        short COMPLETE = 2;
    }
    interface STRING_TASK_STATUS {
        String NEW = "new_task";
        String IN_PROGRESS = "in_progress_task";
        String COMPLETE = "complete_task";
    }
    interface ACTIONS {
        String ADD = "add_item";
        String EDIT = "edit_item";
        String SHARE = Intent.ACTION_SEND;
    }
    String TASK_POS = "task_pos";
    String JSON_DATA = "json_data";
    int DOUBLE_FINISH_CODE = 22;
}
