package org.bitbucket.guwenk.tasktracker.ui.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import org.bitbucket.guwenk.tasktracker.Constants;
import org.bitbucket.guwenk.tasktracker.DataManager;
import org.bitbucket.guwenk.tasktracker.R;
import org.bitbucket.guwenk.tasktracker.Tasks.TaskItem;
import org.bitbucket.guwenk.tasktracker.Tasks.TaskItemAltLook;
import org.bitbucket.guwenk.tasktracker.ui.task_details.TaskDetailsActivity;


public class UpcomingTaskWidget extends AppWidgetProvider {
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

        TaskItem taskItem = new DataManager(context).getUpcomingTask();
        if (taskItem.getStatus() != -1) {
            Intent intentToDetails = new Intent(context, TaskDetailsActivity.class);
            intentToDetails.putExtra(Constants.TASK_POS, new DataManager(context).find_position_by_timestamp(taskItem.getTimeStamp()));
            PendingIntent piToDetails = PendingIntent.getActivity(context, 0, intentToDetails, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.widget_layout, piToDetails);
        } else
            remoteViews.setOnClickPendingIntent(R.id.widget_layout, null);

        TaskItemAltLook taskItemAltLook = new TaskItemAltLook(context, taskItem);
        remoteViews.setTextViewText(R.id.wg_Title, taskItemAltLook.getTitle());
        remoteViews.setTextViewText(R.id.wg_Date, taskItemAltLook.getDate());
        remoteViews.setTextViewText(R.id.wg_Status, taskItemAltLook.getStatus());
        remoteViews.setTextViewText(R.id.wg_Text, taskItemAltLook.getText());
        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
    }
}
