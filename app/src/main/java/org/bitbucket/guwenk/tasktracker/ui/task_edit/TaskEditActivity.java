package org.bitbucket.guwenk.tasktracker.ui.task_edit;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Scroller;
import android.widget.Toast;

import org.bitbucket.guwenk.tasktracker.Constants;
import org.bitbucket.guwenk.tasktracker.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TaskEditActivity extends AppCompatActivity implements TaskEditView, DatePickerDialog.OnDateSetListener {

    private TaskEditPresenter presenter;
    private EditText etTitle;
    private EditText etDate;
    private EditText etText;
    private Button buttonApply;
    private Button buttonDelete;
    private RadioGroup radioGroup;
    private RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_edit);
        etTitle = findViewById(R.id.actTaskEdit_editText_Title);
        etDate = findViewById(R.id.actTaskEdit_editText_Date);
        etText = findViewById(R.id.actTaskEdit_editText_Text);
        buttonApply = findViewById(R.id.actTaskEdit_buttonApply);
        buttonDelete = findViewById(R.id.actTaskEdit_buttonDelete);
        radioGroup = findViewById(R.id.actTaskEdit_radioGroup);
        presenter = new TaskEditPresenter(this, getIntent());

        etText.setScroller(new Scroller(getApplicationContext()));
        etText.setVerticalScrollBarEnabled(true);

        buttonApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onApplyClick(etTitle.getText().toString(), radioGroup.getCheckedRadioButtonId(), etText.getText().toString());
            }
        });
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Constants.DOUBLE_FINISH_CODE);
                presenter.onDeleteClick();
            }
        });

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                new DatePickerDialog(TaskEditActivity.this, TaskEditActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        etDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Calendar calendar = Calendar.getInstance();
                    new DatePickerDialog(TaskEditActivity.this, TaskEditActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });

        if (getIntent().getAction().equals(Constants.ACTIONS.EDIT))
            presenter.loadDataByPosition();
    }

    @Override
    public void wrongTitle() {
        etTitle.setError(getString(R.string.wrong_value));
    }

    @Override
    public void wrongDate() {
        etDate.setError(getString(R.string.wrong_value));
    }

    @Override
    public void showShortToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void updateTitle(String title) {
        etTitle.setText(title);
    }

    @Override
    public void updateDate(GregorianCalendar calendar) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        etDate.setText(dateFormat.format(calendar.getTime()));
    }

    @Override
    public void updateStatus(int status) {
        switch (status) {
            case 1:
                radioButton = findViewById(R.id.actTaskEdit_radioBtn_inProgress);
                break;
            case 2:
                radioButton = findViewById(R.id.actTaskEdit_radioBtn_complete);
                break;
            default:
                radioButton = findViewById(R.id.actTaskEdit_radioBtn_new);
                break;
        }
        radioButton.setChecked(true);
    }

    @Override
    public void updateText(String text) {
        etText.setText(text);
    }

    @Override
    public void buttonDeleteVisibility(int visibility) {
        buttonDelete.setVisibility(visibility);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        presenter.onDateEditClick(new GregorianCalendar(year, month, dayOfMonth));
        etDate.setText(String.format("%02d.%02d.%d", dayOfMonth, month + 1, year));
        etText.requestFocus();
    }
}
