package org.bitbucket.guwenk.tasktracker.ui.task_details;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import org.bitbucket.guwenk.tasktracker.Constants;
import org.bitbucket.guwenk.tasktracker.DataManager;
import org.bitbucket.guwenk.tasktracker.R;
import org.bitbucket.guwenk.tasktracker.Tasks.TaskItem;
import org.bitbucket.guwenk.tasktracker.Tasks.TaskItemAltLook;
import org.bitbucket.guwenk.tasktracker.ui.task_edit.TaskEditActivity;

import java.util.ArrayList;

public class TaskDetailsActivity extends AppCompatActivity {

    int position;
    private TextView tvTitle;
    private TextView tvDate;
    private TextView tvStatus;
    private TextView tvText;
    private Button buttonOK;
    private Button buttonLVLup;
    private Button buttonEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

        tvTitle = findViewById(R.id.actTaskDetails_textView_Title);
        tvDate = findViewById(R.id.actTaskDetails_textView_Date);
        tvStatus = findViewById(R.id.actTaskDetails_textView_Status);
        tvText = findViewById(R.id.actTaskDetails_textView_Text);
        buttonOK = findViewById(R.id.actTaskDetails_button_ok);
        buttonLVLup = findViewById(R.id.actTaskDetails_button_lvlup);
        buttonEdit = findViewById(R.id.actTaskDetails_button_edit);
        position = getIntent().getExtras().getInt(Constants.TASK_POS);

        tvText.setScroller(new Scroller(getApplicationContext()));
        tvText.setVerticalScrollBarEnabled(true);


        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonLVLup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<TaskItem> taskItems = new DataManager(getApplicationContext()).getTaskList();
                if (taskItems.get(position).getStatus() < 2) {
                    taskItems.get(position).setStatus(taskItems.get(position).getStatus() + 1);
                    new DataManager(getApplicationContext()).saveTaskList(taskItems);
                    update_data();
                } else
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.task_is_already_completed), Toast.LENGTH_SHORT).show();
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaskDetailsActivity.this, TaskEditActivity.class);
                intent.putExtra(Constants.TASK_POS, position);
                intent.setAction(Constants.ACTIONS.EDIT);
                startActivityForResult(intent, Constants.DOUBLE_FINISH_CODE);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == resultCode) finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        update_data();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int pos_temp = intent.getExtras().getInt(Constants.TASK_POS, -1);
        position = pos_temp != -1 ? pos_temp : position;
        update_data();
    }

    void update_data() {
        TaskItem task = new DataManager(getApplicationContext()).getTaskList().get(position);
        TaskItemAltLook taskAlt = new TaskItemAltLook(this, task);
        tvTitle.setText(taskAlt.getTitle());
        tvDate.setText(taskAlt.getDate());
        tvStatus.setText(taskAlt.getStatus());
        tvText.setText(taskAlt.getText());
    }
}
