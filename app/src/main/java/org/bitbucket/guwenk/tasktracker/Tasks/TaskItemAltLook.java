package org.bitbucket.guwenk.tasktracker.Tasks;

import android.annotation.SuppressLint;
import android.content.Context;

import org.bitbucket.guwenk.tasktracker.R;

import java.text.SimpleDateFormat;

public class TaskItemAltLook {
    private String title;
    private String date;
    private String status;
    private String text;

    public TaskItemAltLook(Context context, TaskItem taskItem){
        title = taskItem.getTitle();
        switch (taskItem.getStatus()) {
            case -1:
                status = "";
                break;
            case 1:
                status = context.getResources().getString(R.string.status_in_progress);
                break;
            case 2:
                status = context.getResources().getString(R.string.status_complete);
                break;
            default:
                status = context.getResources().getString(R.string.status_new);
        }

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        if (taskItem.getDate().getTimeInMillis() != Long.MAX_VALUE)
            date = dateFormat.format(taskItem.getDate().getTime());
        else date = "";
        text = taskItem.getText();
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public String getText() {
        return text;
    }
}
