package org.bitbucket.guwenk.tasktracker;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.bitbucket.guwenk.tasktracker.Tasks.TaskItem;
import org.bitbucket.guwenk.tasktracker.ui.widget.UpcomingTaskWidget;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class DataManager {
    private Context context;

    public DataManager(Context context) {
        this.context = context;
    }

    public ArrayList<TaskItem> getTaskList() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(Constants.JSON_DATA, "");
        if (json.isEmpty()) {
            return new ArrayList<>();
        }
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<TaskItem>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveTaskList(ArrayList<TaskItem> taskItemArrayList) {
        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(taskItemArrayList);
        prefsEditor.putString(Constants.JSON_DATA, json);
        prefsEditor.apply();

        updateWidget();
    }

    public int find_position_by_timestamp(long timestamp) {
        ArrayList<TaskItem> taskList = getTaskList();
        int sourcePosition = -1;
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getTimeStamp() == timestamp) {
                sourcePosition = i;
                break;
            }
        }
        return sourcePosition;
    }

    public TaskItem getUpcomingTask() {
        ArrayList<TaskItem> taskList = getTaskList();

        TaskItem task_result = new TaskItem();
        task_result.setTitle(context.getString(R.string.no_upcoming_tasks));
        task_result.setStatus(-1);
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(Long.MAX_VALUE);
        task_result.setDate(gregorianCalendar);
        task_result.setText("");

        if (taskList.size() > 0) {
            for (TaskItem task_temp : taskList) {
                if (task_temp.getStatus() < 2) {
                    if (task_temp.getDate().getTimeInMillis() < task_result.getDate().getTimeInMillis()) {
                        task_result = task_temp;
                    }
                }
            }
        }
        return task_result;
    }

    private void updateWidget() {

        Intent intent = new Intent(context, UpcomingTaskWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, UpcomingTaskWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        context.sendBroadcast(intent);
    }
}
