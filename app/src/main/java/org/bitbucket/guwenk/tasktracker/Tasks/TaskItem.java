package org.bitbucket.guwenk.tasktracker.Tasks;

import java.util.GregorianCalendar;

public class TaskItem {
    private String title;
    private GregorianCalendar date;
    private int status;
    private String text;
    private long timeStamp;

    public TaskItem() {
        timeStamp = System.currentTimeMillis();
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public GregorianCalendar getDate() {
        return date;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
