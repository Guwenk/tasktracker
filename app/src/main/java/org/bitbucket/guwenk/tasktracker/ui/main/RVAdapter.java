package org.bitbucket.guwenk.tasktracker.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.bitbucket.guwenk.tasktracker.Constants;
import org.bitbucket.guwenk.tasktracker.R;
import org.bitbucket.guwenk.tasktracker.Tasks.TaskItem;
import org.bitbucket.guwenk.tasktracker.Tasks.TaskItemAltLook;

import java.util.ArrayList;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ViewHolder> implements Filterable {

    private Context context;
    private ArrayList<TaskItem> sourceTasks;
    private ArrayList<TaskItem> filteredTasks;
    private OnItemClickListener mListener;

    RVAdapter(Context context, ArrayList<TaskItem> tasks) {
        this.sourceTasks = tasks;
        this.filteredTasks = tasks;
        this.context = context;
    }

    public ArrayList<TaskItem> getFilteredTasks() {
        return filteredTasks;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.taskbox_item, parent, false);
        return new ViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TaskItemAltLook taskItemAltLook = new TaskItemAltLook(context, filteredTasks.get(position));

        holder.title.setText(taskItemAltLook.getTitle());
        holder.date.setText(taskItemAltLook.getDate());
        holder.status.setText(taskItemAltLook.getStatus());
        holder.text.setText(taskItemAltLook.getText());
    }

    @Override
    public int getItemCount() {
        return filteredTasks.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String s = constraint.toString();
                int status;
                switch (s) {
                    case Constants.STRING_TASK_STATUS.NEW:
                        status = 0;
                        break;
                    case Constants.STRING_TASK_STATUS.IN_PROGRESS:
                        status = 1;
                        break;
                    case Constants.STRING_TASK_STATUS.COMPLETE:
                        status = 2;
                        break;
                    default:
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = sourceTasks;
                        return filterResults;
                }

                filteredTasks = new ArrayList<>();
                for (int i = 0; i < sourceTasks.size(); i++) {
                    if (sourceTasks.get(i).getStatus() == status) {
                        filteredTasks.add(sourceTasks.get(i));
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredTasks;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredTasks = (ArrayList<TaskItem>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView date;
        TextView status;
        TextView text;

        ViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);
            title = itemView.findViewById(R.id.cardViewTitle);
            date = itemView.findViewById(R.id.cardViewDate);
            status = itemView.findViewById(R.id.cardViewStatus);
            text = itemView.findViewById(R.id.cardViewText);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}