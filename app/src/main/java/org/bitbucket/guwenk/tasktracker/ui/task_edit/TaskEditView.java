package org.bitbucket.guwenk.tasktracker.ui.task_edit;

import android.content.Context;

import java.util.GregorianCalendar;

interface TaskEditView {
    void wrongTitle();
    void wrongDate();
    void showShortToast(String message);
    void finishActivity();
    Context getAppContext();
    void updateTitle(String title);
    void updateDate(GregorianCalendar calendar);
    void updateStatus(int status);
    void updateText(String text);
    void buttonDeleteVisibility(int visibility);
}
